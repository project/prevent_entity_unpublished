# Prevent Entity Unpublished

To prevent entity to being unpublished if referenced to another entity


## CONTENTS OF THIS FILE

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

Drupal 8.8 and above
[Entity Reference Integrity, Entity Reference Integrity Inforce] Contrib module


## Installation

The installation of this module is like other Drupal modules.
If your site is [managed via Composer](https://www.drupal.org/node/2718229),
use Composer to download the webform module running ```composer require "drupal/prevent_entity_unpublished"```.
Otherwise copy/upload the module to the modules directory of your Drupal installation.


## Configuration

Enable [Entity Reference Integrity, Entity Reference Integrity Enforce](https://www.drupal.org/project/entity_reference_integrity) module.
Install and enable Prevent Entity Unpublish.
Go to setting page for an entity reference field and select entity for "Unpublished content"
for the Reference method.


## Maintainers

- Yogita Rathod - [yogita](https://www.drupal.org/u/yogitar-0)
