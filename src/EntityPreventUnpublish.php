<?php

namespace Drupal\prevent_entity_unpublish;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\entity_reference_integrity\EntityReferenceDependencyManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Hook into entity update and throw an exception to prevent them disappearing.
 */
class EntityPreventUnpublish implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The dependency manager.
   *
   * @var \Drupal\entity_reference_integrity\EntityReferenceDependencyManagerInterface
   */
  protected $dependencyManager;

  /**
   * The entity type IDs protection is enabled for.
   *
   * @var array
   */
  protected $enabledEntityTypeIds;

  /**
   * Drupal\Core\Render\RendererInterface definition.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Create a DeleteFormAlter object.
   *
   * @param Drupal\entity_reference_integrity\EntityReferenceDependencyManagerInterface $calculator
   *   The dependency manager.
   * @param array $enabled_entity_type_ids
   *   The entity type ids.
   * @param Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(EntityReferenceDependencyManagerInterface $calculator, array $enabled_entity_type_ids, RendererInterface $renderer) {
    $this->dependencyManager = $calculator;
    $this->enabledEntityTypeIds = $enabled_entity_type_ids;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_reference_integrity.dependency_manager'),
      !is_null($container->get('config.factory')->get('prevent_entity_unpublish.settings')->get('enabled_entity_type_ids')) ? $container->get('config.factory')->get('prevent_entity_unpublish.settings')->get('enabled_entity_type_ids') : [], $container->get('renderer')
    );
  }

  /**
   * Implements entity update.
   */
  public function entityUpdate(EntityInterface $entity, FormStateInterface $form_state) {
    if (in_array($entity->getEntityTypeId(), $this->enabledEntityTypeIds, TRUE) && $this->dependencyManager->hasDependents($entity)) {
      $referencing_entities = $this->dependencyManager->getDependentEntities($entity);
      $output = $this->getEntityList($referencing_entities);
      $status = '';
      if ($entity->getEntityTypeId() == 'user') {
        $status = 'disable';
      }
      else {
        $status = 'unpublish';
      }
      $form_state->setErrorByName('status', $this->t('Can not %s "%a" of type "%b" with label "%c" because other <br/> %e is referencing it and the integrity of this entity type is enforced.',
        [
          "%s" => $status,
          "%a" => $entity->getEntityTypeId(),
          "%b" => $entity->bundle(),
          "%c" => $entity->label(),
          "%d" => $entity->id(),
          "%e" => $output,
        ]
      ));
    }
  }

  /**
   * Implements get all entity list.
   */
  public function getEntityList($referencing_entities) {
    $build = [];
    if ($referencing_entities) {
      foreach ($referencing_entities as $entity_type_id => $rentities) {
        $build[$entity_type_id]['label'] = [
          '#type' => 'html_tag',
          '#tag' => 'strong',
          '#value' => reset($rentities)->getEntityType()->getLabel(),
        ];
        $build[$entity_type_id]['list'] = [
          '#theme' => 'item_list',
          '#items' => [],
        ];
        foreach ($rentities as $rentity) {
          $build[$entity_type_id]['list']['#items'][] = $rentity->hasLinkTemplate('canonical') ? $rentity->toLink() : $rentity->label();
        }
      }
    }
    return $this->renderer->render($build);
  }

}
