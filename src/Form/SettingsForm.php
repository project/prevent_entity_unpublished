<?php

namespace Drupal\prevent_entity_unpublish\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The settings form for prevent_entity_unpublish.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * An array of entity type definitions.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface[]
   */
  protected $entityTypeDefinitions;

  /**
   * Create a SettingsForm.
   */
  public function __construct(ConfigFactoryInterface $config_factory, $entity_type_definitions) {
    parent::__construct($config_factory);
    $this->entityTypeDefinitions = $entity_type_definitions;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')->getDefinitions()
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['prevent_entity_unpublish.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'prevent_entity_unpublish_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $options = [];
    foreach ($this->entityTypeDefinitions as $definition) {
      if ($definition->id() == 'node' || $definition->id() == 'taxonomy_term' || $definition->id() == 'user') {
        $options[$definition->id()] = $definition->getLabel() ?: $definition->id();
      }
    }
    uasort($options, 'strcasecmp');
    $form['intro'] = [
      '#markup' => '<p>' . $this->t('Select the entity types which, when referenced will be prevented from being unpublished.') . '</p>',
    ];
    $form['enabled_entity_type_ids'] = [
      '#title' => $this->t('Enabled Entities'),
      '#type' => 'checkboxes',
      '#multiple' => TRUE,
      '#options' => $options,
      '#default_value' => $this->config('prevent_entity_unpublish.settings')->get('enabled_entity_type_ids') ? $this->config('prevent_entity_unpublish.settings')->get('enabled_entity_type_ids') : [],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this
      ->config('prevent_entity_unpublish.settings')
      ->set('enabled_entity_type_ids', $form_state->getValue('enabled_entity_type_ids'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
